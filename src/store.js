import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    /*
    sclubBaseUrl: 'http://localhost:8080/mobile/',
    sclubImageUrl: 'http://localhost:8080/mobile/img/',
    lawketBaseUrl: 'http://localhost:8080/lawket_php/',
    lawketImageUrl: 'http://localhost:8080/lawket_php/images/',
    baekyangBaseUrl: 'http://newbird15.cafe24.com/data/app_php/',
    */
    lawketBaseUrl: 'http://192.168.1.220:8080/lawket_php/',
    lawketImageUrl: 'http://192.168.1.220:8080/lawket_php/images/',
    baekyangBaseUrl: 'http://newbird15.cafe24.com/data/app_php/',

    adminFlag: false,                     //for login with Admin
    adminData: [],                        //for login with Administrator content
    legalOfficeFlag: false,               //for login with legalOffice
    legalOfficeData: [],                  //for login with legalOffice content
    check_terms: {
      lo_terms: '',       //이용약관
      lo_offer: '',       //개인정보 제3자 제공
      lo_marketing: '',   //마케팅 정보수신
    },
    top_search: {
      lo_description: '', //search target for 통합검색
      lo_name: '',        //search target for 업체명검색
      search_mode: '',    // 'name' or 'description'
    },
  },
});