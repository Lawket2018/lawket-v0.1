import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import CustomSearch from '@/components/CustomSearch'
import SearchByRegion from '@/components/SearchByRegion'
import SearchByProduct from '@/components/SearchByProduct'
import RealTimeConsult from '@/components/RealTimeConsult'
import AddConsult from '@/components/AddConsult'
import News from '@/components/Community/News'
import Qna from '@/components/Community/Qna'
import Tips from '@/components/Community/Tips'
import UseGuide from '@/components/UseGuide'
import ServiceCenter from '@/components/ServiceCenter'
import CompanyIntro from '@/components/CompanyIntro'
import CreateCustomerStep1 from '@/components/CreateCustomer/CreateCustomerStep1'
import CreateCustomerStep2 from '@/components/CreateCustomer/CreateCustomerStep2'
import UpdateCustomer from '@/components/CreateCustomer/UpdateCustomer'

import CreateAdmin from '@/components/Admin/CreateAdmin'
import CreateAdType from '@/components/Admin/CreateAdType'
import CreateInfomation from '@/components/Admin/CreateInfomation'
import CreateConsult from '@/components/Admin/CreateConsult'
import CreateFaq from '@/components/Admin/CreateFaq'
import CreateNotice from '@/components/Admin/CreateNotice'
import CreateCustomer from '@/components/Admin/CreateCustomer'
import RequestAd from '@/components/Admin/RequestAd'
import CreateTerms from '@/components/Admin/CreateTerms'
//import UploadFile from '@/components/Admin/UploadFile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/CustomSearch',
      name: 'CustomSearch',
      component: CustomSearch
    },
    {
      path: '/SearchByRegion',
      name: 'SearchByRegion',
      component: SearchByRegion
    },
    {
      path: '/SearchByProduct',
      name: 'SearchByProduct',
      component: SearchByProduct
    },
    {
      path: '/RealTimeConsult',
      name: 'RealTimeConsult',
      component: RealTimeConsult
    },,
    {
      path: '/AddConsult',
      name: 'AddConsult',
      component: AddConsult
    },
    {
      path: '/Community/News',
      name: 'News',
      component: News
    },
    {
      path: '/Community/Qna',
      name: 'Qna',
      component: Qna
    },
    {
      path: '/Community/Tips',
      name: 'Tips',
      component: Tips
    },
    {
      path: '/UseGuide',
      name: 'UseGuide',
      component: UseGuide
    },
    {
      path: '/ServiceCenter',
      name: 'ServiceCenter',
      component: ServiceCenter
    },
    {
      path: '/CompanyIntro',
      name: 'CompanyIntro',
      component: CompanyIntro
    },
    {
      path: '/CreateCustomer/CreateCustomerStep1',
      name: 'CreateCustomerStep1',
      component: CreateCustomerStep1
    },
    {
      path: '/CreateCustomer/CreateCustomerStep2',
      name: 'CreateCustomerStep2',
      component: CreateCustomerStep2
    },
    {
      path: '/CreateCustomer/UpdateCustomer',
      name: 'UpdateCustomer',
      component: UpdateCustomer
    },
    {
      path: '/Admin/CreateAdmin',
      name: 'CreateAdmin',
      component: CreateAdmin
    },
    {
      path: '/Admin/CreateAdType',
      name: 'CreateAdType',
      component: CreateAdType
    },
    {
      path: '/Admin/CreateInfomation',
      name: 'CreateInfomation',
      component: CreateInfomation
    },
    {
      path: '/Admin/CreateConsult',
      name: 'CreateConsult',
      component: CreateConsult
    },
    {
      path: '/Admin/CreateFaq',
      name: 'CreateFaq',
      component: CreateFaq
    },
    {
      path: '/Admin/CreateNotice',
      name: 'CreateNotice',
      component: CreateNotice
    },
    {
      path: '/Admin/CreateCustomer',
      name: 'CreateCustomer',
      component: CreateCustomer
    },
    {
      path: '/Admin/RequestAd',
      name: 'RequestAd',
      component: RequestAd
    },
    {
      path: '/Admin/CreateTerms',
      name: 'CreateTerms',
      component: CreateTerms
    }
  ]
})